#!/bin/bash -e

# Source the ofm-init script from .bashrc, so it works for both the 
# first user and for root.

mkdir -p files
wget https://gitlab.com/openflexure/openflexure-microscope-cli/-/raw/master/ofm -O files/ofm
wget https://gitlab.com/openflexure/openflexure-microscope-cli/-/raw/master/ofm-init -O files/ofm-init

install -v -m 755 files/ofm-init "${ROOTFS_DIR}/usr/bin/ofm-init"
install -v -m 755 files/ofm "${ROOTFS_DIR}/usr/bin/ofm"

echo "source /usr/bin/ofm-init" >> "${ROOTFS_DIR}/home/${FIRST_USER_NAME}/.bashrc"
echo "source /usr/bin/ofm-init" >> "${ROOTFS_DIR}/root/.bashrc"
