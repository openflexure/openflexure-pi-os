#!/bin/bash -e

# Download and install the OFM software

# "Product" information
disturl="https://build.openflexure.org/openflexure-microscope-server/latest"

# Export important variables
libpath="/var/openflexure/application/openflexure-microscope-server"
virtualenvdir="/var/openflexure/application/openflexure-microscope-server/.venv"

# Create server user, group, path
env
echo "Adding OF user ${OPENFLEXURE_USER_NAME}"
on_chroot << EOF
adduser --system "${OPENFLEXURE_USER_NAME}"
groupadd -f "${OPENFLEXURE_GROUP_NAME}"

usermod -g "${OPENFLEXURE_GROUP_NAME}" "${OPENFLEXURE_USER_NAME}"
usermod -a -G "${OPENFLEXURE_GROUP_NAME}" "${FIRST_USER_NAME}"
EOF

# Add openflexure-ws user to required groups
# https://raspberrypi.stackexchange.com/questions/70214/what-does-each-of-the-default-groups-on-the-raspberry-pi-do
# User needs to be in sudo group to enable shutdown and restart API actions
for GRP in adm dialout audio users video plugdev input gpio spi i2c netdev sudo; do
	on_chroot <<< "adduser '$OPENFLEXURE_USER_NAME' $GRP"
done
# User needs paswordless executable permissions to /usr/sbin/shutdown to enable shutdown and restart API actions
echo "${OPENFLEXURE_USER_NAME} ${TARGET_HOSTNAME} =NOPASSWD: /usr/sbin/shutdown" | tee -a "${ROOTFS_DIR}/etc/sudoers"

mkdir -v -p "${ROOTFS_DIR}/var/openflexure"

# Create repo directory
mkdir -v -p "${ROOTFS_DIR}$libpath"

# Create virtualenv directory
on_chroot << EOF
echo -e "\nCreating virtual environment in $virtualenvdir"
python3 -m venv "$virtualenvdir" --prompt "OFM Server"

echo "Activating virtualenv..."
# shellcheck disable=SC1090
source "$virtualenvdir/bin/activate"

# Install additional deps
echo -e "Installing pipenv and wheel to allow installation of the server."
$virtualenvdir/bin/python -m pip install --upgrade pipenv wheel pip setuptools

# Download and extract server
wget -qO- $disturl | tar xvz -C $libpath

# Work in repo folder
cd "$libpath" || exit 1

# Install server and dependencies
pipenv install --deploy

# Deactivate server environment
deactivate

# Ensure ownership
chown -R "${OPENFLEXURE_USER_NAME}:${OPENFLEXURE_GROUP_NAME}" /var/openflexure

# Install service
echo "Enabling openflexure-microscope-server service..."

echo | tee -a  /etc/systemd/system/openflexure-microscope-server.service <<- EOL
[Unit]
Description=Flask instance to serve the OpenFlexure Microscope API
After=network.target

[Service]
User=${OPENFLEXURE_USER_NAME}
Group=${OPENFLEXURE_GROUP_NAME}
WorkingDirectory=/var/openflexure
Environment="PATH=$virtualenvdir/bin/:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin"
ExecStart=$virtualenvdir/bin/python -m openflexure_microscope.api.app

[Install]
WantedBy=multi-user.target
EOL

systemctl enable openflexure-microscope-server
EOF